import time


class Timeing:
    def __init__(self, message):
        self.message = message

    def __enter__(self):
        print('start to ' + self.message + '...')
        self.start_time = time.time()
        return self.start_time

    def __exit__(self, type, value, traceback):
        end_time = int(round((time.time() - self.start_time) * 1000))
        print('...' + str(end_time))
