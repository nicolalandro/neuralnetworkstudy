import keras
from keras import Sequential
from keras.layers import Dense, Conv2D, Activation, MaxPooling2D, Flatten


# INPUT => CONV => RELU => POOL => CONV => RELU => POOL => FC => RELU => FC
class LeNet:
    def __init__(self, input_shape, output_shape, batch_size=128, epochs=500):
        self.batch_size = batch_size
        self.epochs = epochs

        activation = "relu"
        # initialize the model
        self.model = Sequential()
        # define the first set of CONV => ACTIVATION => POOL layers
        self.model.add(Conv2D(32, kernel_size=(3, 3), padding="same", input_shape=input_shape))
        self.model.add(Activation(activation))
        self.model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
        # define the second set of CONV => ACTIVATION => POOL layers
        self.model.add(Conv2D(50, kernel_size=(3, 3), padding="same"))
        self.model.add(Activation(activation))
        self.model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
        # define the first FC => ACTIVATION layers
        self.model.add(Flatten())
        self.model.add(Dense(500))
        self.model.add(Activation(activation))
        # define the second FC layer
        self.model.add(Dense(output_shape))
        # lastly, define the soft-max classifier
        self.model.add(Activation("softmax"))

        self.model.compile(loss=keras.losses.sparse_categorical_crossentropy,
                           optimizer=keras.optimizers.SGD(lr=0.01, momentum=0.9, nesterov=True))

    def fit(self, x, y):
        self.model.fit(x, y, epochs=self.epochs, batch_size=self.batch_size)

    def predict(self, x):
        return self.model.predict(x, batch_size=self.batch_size).argmax(1)

    def predict_proba(self, x):
        return self.model.predict(x, batch_size=self.batch_size)

    def get_keras_model(self):
        return self.model

    def save(self, json_path, h5_path):
        model_json = self.model.to_json()
        with open(json_path, "w") as json_file:
            json_file.write(model_json)
        self.model.save_weights(h5_path)

    def load(self, h5_path):
        self.model.load_weights(h5_path)

    def summary(self):
        self.model.summary()
