import sys
sys.path.insert(0, '../../../') # I do not know why

from src.cnn.le_net import LeNet
from src.util.timeing import Timeing
from keras.datasets import mnist

if __name__ == '__main__':
    with Timeing('read data') as _:
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        x_train = x_train.reshape((x_train.shape[0], 28, 28, 1))
        x_test = x_test.reshape((x_test.shape[0], 28, 28, 1))
        x_train = x_train.astype("float32") / 255.0
        x_test = x_test.astype("float32") / 255.0
    with Timeing('train'):
        model = LeNet(x_train[0].shape, 10)
        model.summary()
        model.fit(x_train, y_train)

    with Timeing('save'):
        model.save(
            '../../assets/mnist/le_net.json',
            '../../assets/mnist/le_net.h5'
        )
